<?php

class installation_image_views_handler extends views_handler_field {
   function render($values) {
    // dsm($values);
     $data_id = $values->reg_id;
     $img = get_image_for_views_handler($values->file_managed_user_reg_uri, $data_id);
     return $img;
   }
}
/*
  * Get Formatted Image for views handler
  */
function get_image_for_views_handler($uri, $data_id = 0)
{
   $img = '';
   if (!empty($uri)) {
     $image_uri = $uri;
     $file = drupal_realpath($image_uri);
     //dsm($file);
     if (fileExists($file)) {
       $img = '<div class="impl-image"><a href="' .file_create_url($image_uri) . '" rel="data-' . $data_id . '"class="fancybox"><img src="' . image_style_url('medium', $image_uri). '"></a></ div>';
     }
     else {
       $img = '<div class="impl-image"><img src="' .file_create_url('public://no-image-available.png') . '"></div>';
     }
   }
   else {
     $img = '<div class="impl-image"><img src="' .file_create_url('public://no-image-available.png') . '"></div>';
   }
   return $img;
}

function fileExists($file){
  
  return true;
}