<?php

function register_views_data() {
  // Basic table information.
  $data['user_reg']['table']['group'] = t('Register user DataTable');
  $data['user_reg']['table']['base'] = array(
    // This is the identifier field for the view.
    'field' => 'reg_id',
    'title' => t('Register user DataTable'),
    'help' => t('User Form table contains example content and can be related to nodes.'),
    'weight' => -10,
  );
  $data['user_reg']['reg_id'] = array(
    'title' => t('User ID'),
    'help' => t('Just a numeric field.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  //  first_name text field.
  $data['user_reg']['first_name'] = array(
    'title' => t('First Name'),
    'help' => t('Just a plain text field.'),
    'field' => array(
      'handler' => 'views_handler_field',
      // This is use by the table display plugin.
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  //  last_name text field.
  $data['user_reg']['last_name'] = array(
    'title' => t('Last Name'),
    'help' => t('Just a plain text field.'),
    'field' => array(
      'handler' => 'views_handler_field',
      // This is use by the table display plugin.
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  // Date of Birth field
  $data['user_reg']['dob'] = array(
    'title' => t('Date of Birth'),
    'help' => t('Just a Date field.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      // This is use by the table display plugin.
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),  
    'filter' => array(
      'handler' => 'views_handler_filter_simple',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_date',
    ),
  );

  // Example Address field.
  $data['user_reg']['address'] = array(
    'title' => t('Address'),
    'help' => t('Just a Address field.'),
    'field' => array(
      'handler' => 'views_handler_field',
      // This is use by the table display plugin.
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // City field
    $data['user_reg']['city'] = array(
    'title' => t('City'),
    'help' => t('Just a city field.'),
    'field' => array(
      'handler' => 'views_handler_field',
      // This is use by the table display plugin.
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
     // Example Gender field.
    $data['user_reg']['gender'] = array(
    'title' => t('Gender'),
    'help' => t('Just a Gender field.'),
    'field' => array(
      'handler' => 'views_handler_field',
      // This is use by the table display plugin.
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
    
  // Image field.
  $data['user_reg']['upload'] = array(
    'title' => t('Uploaded image'),
    'help' => t('Just a Image field.'),
    'field' => array(
      'handler' => 'installation_image_views_handler',
      // This is use by the table display plugin.
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_numeric',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_simple',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'relationship' => array(
      'base' => 'file_managed', // The name of the table to join with.
      'base field' => 'fid', // The name of the field on the joined table.
      'label' => t('UserImage relationship for reg_id'),
      'title' => t('UserImage relationship for reg_id'),
      'help' => t('UserImage relationship for reg_id'),
    ),
  );
  
   // Example Image Display Field.
    $data['user_reg']['image_field'] = array(
    'title' => t('Image Field'),
    'help' => t('Just a image field.'),
    'field' => array(
      'handler' => 'installation_image_views_handler',
      // This is use by the table display plugin.
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

    
   // Created at field.
  $data['user_reg']['created'] = array(
    'title' => t('Created At'),
    'help' => t('Just a Date field.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      // This is use by the table display plugin.
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_simple',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_date',
    ),
  );
   
  
   // Created by field.
  $data['user_reg']['created_by'] = array(
    'title' => t('Created By'),
    'help' => t('Just a Date field.'),
    'field' => array(
      'handler' => 'views_handler_field',
      // This is use by the table display plugin.
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  
    // Updated at field.
  $data['user_reg']['updated'] = array(
    'title' => t('Updated At'),
    'help' => t('Just a Date field.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      // This is use by the table display plugin.
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(  
      'handler' => 'views_handler_filter_simple',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_date',
    ),
  );
  // Updated by field.
  $data['user_reg']['updated_by'] = array(
    'title' => t('Updated By'),
    'help' => t('Just a Date field.'),
    'field' => array(
      'handler' => 'views_handler_field',
      // This is use by the table display plugin.
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  
  
  return $data;
}


