<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$view = new view();
$view->name = 'sit_and_eat_pending_recce_approval';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'recce';
$view->human_name = 'Sit And Eat Pending Recce Approval';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Sit And Eat Pending Recce Audit Approval';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['group_by'] = TRUE;
$handler->display->display_options['access']['type'] = 'role';
$handler->display->display_options['access']['role'] = array(
  3 => '3',
  9 => '9',
  8 => '8',
  6 => '6',
  7 => '7',
  5 => '5',
);
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '10';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['pager']['options']['id'] = '0';
$handler->display->display_options['pager']['options']['quantity'] = '9';
$handler->display->display_options['style_plugin'] = 'footable';
$handler->display->display_options['style_options']['columns'] = array(
  'views_bulk_operations' => 'views_bulk_operations',
  'shop_id' => 'shop_id',
  'outlet_name' => 'outlet_name',
  'district' => 'district',
  'created' => 'created',
  'shop_image_id' => 'shop_image_id',
  'recce_image_id' => 'recce_image_id',
  'recce_field1' => 'recce_field1',
  'recce_width' => 'recce_width',
  'recce_height' => 'recce_height',
  'latitude' => 'latitude',
  'longitude' => 'longitude',
  'recce_id' => 'recce_id',
  'recce_field2' => 'recce_field2',
  'status' => 'status',
  'recce_field5' => 'recce_field5',
  'recce_field3' => 'recce_field3',
  'status2' => 'status2',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'views_bulk_operations' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'shop_id' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'outlet_name' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'district' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'created' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'shop_image_id' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'recce_image_id' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'recce_field1' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'recce_width' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'recce_height' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'latitude' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'longitude' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'recce_id' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'recce_field2' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'status' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'recce_field5' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'recce_field3' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'status2' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
$handler->display->display_options['style_options']['empty_table'] = TRUE;
$handler->display->display_options['style_options']['footable'] = array(
  'expand' => 'outlet_name',
  'icon' => '',
  'icon_size' => '',
  'hide' => array(
    'views_bulk_operations' => array(
      'phone' => 0,
      'tablet' => 0,
    ),
    'shop_id' => array(
      'phone' => 0,
      'tablet' => 0,
    ),
    'outlet_name' => array(
      'phone' => 0,
      'tablet' => 0,
    ),
    'district' => array(
      'phone' => 'phone',
      'tablet' => 'tablet',
    ),
    'created' => array(
      'phone' => 'phone',
      'tablet' => 'tablet',
    ),
    'shop_image_id' => array(
      'phone' => 0,
      'tablet' => 0,
    ),
    'recce_image_id' => array(
      'phone' => 'phone',
      'tablet' => 'tablet',
    ),
    'recce_field1' => array(
      'phone' => 'phone',
      'tablet' => 'tablet',
    ),
    'recce_width' => array(
      'phone' => 'phone',
      'tablet' => 'tablet',
    ),
    'recce_height' => array(
      'phone' => 'phone',
      'tablet' => 'tablet',
    ),
    'latitude' => array(
      'phone' => 0,
      'tablet' => 0,
    ),
    'longitude' => array(
      'phone' => 0,
      'tablet' => 0,
    ),
    'recce_id' => array(
      'phone' => 0,
      'tablet' => 0,
    ),
    'recce_field2' => array(
      'phone' => 'phone',
      'tablet' => 'tablet',
    ),
    'status' => array(
      'phone' => 'phone',
      'tablet' => 'tablet',
    ),
    'recce_field5' => array(
      'phone' => 'phone',
      'tablet' => 'tablet',
    ),
    'recce_field3' => array(
      'phone' => 'phone',
      'tablet' => 'tablet',
    ),
    'status2' => array(
      'phone' => 'phone',
      'tablet' => 'tablet',
    ),
  ),
);
/* Header: Global: Result summary */
$handler->display->display_options['header']['result']['id'] = 'result';
$handler->display->display_options['header']['result']['table'] = 'views';
$handler->display->display_options['header']['result']['field'] = 'result';
$handler->display->display_options['header']['result']['content'] = '<div class="text-right"><strong>Displaying @start - @end of @total</strong></div>';
/* No results behavior: Global: Text area */
$handler->display->display_options['empty']['area']['id'] = 'area';
$handler->display->display_options['empty']['area']['table'] = 'views';
$handler->display->display_options['empty']['area']['field'] = 'area';
$handler->display->display_options['empty']['area']['empty'] = TRUE;
$handler->display->display_options['empty']['area']['content'] = '<strong>No result found.</strong>';
$handler->display->display_options['empty']['area']['format'] = 'filtered_html';
/* Relationship: Recce: Recce - Shop */
$handler->display->display_options['relationships']['shop_id']['id'] = 'shop_id';
$handler->display->display_options['relationships']['shop_id']['table'] = 'recce';
$handler->display->display_options['relationships']['shop_id']['field'] = 'shop_id';
$handler->display->display_options['relationships']['shop_id']['required'] = TRUE;
/* Relationship: Shop: Shop - Village */
$handler->display->display_options['relationships']['village_id']['id'] = 'village_id';
$handler->display->display_options['relationships']['village_id']['table'] = 'shop';
$handler->display->display_options['relationships']['village_id']['field'] = 'village_id';
$handler->display->display_options['relationships']['village_id']['relationship'] = 'shop_id';
$handler->display->display_options['relationships']['village_id']['required'] = TRUE;
/* Relationship: Village: Village - States */
$handler->display->display_options['relationships']['state_id']['id'] = 'state_id';
$handler->display->display_options['relationships']['state_id']['table'] = 'village';
$handler->display->display_options['relationships']['state_id']['field'] = 'state_id';
$handler->display->display_options['relationships']['state_id']['relationship'] = 'village_id';
$handler->display->display_options['relationships']['state_id']['required'] = TRUE;
/* Relationship: Shop: Shop - customer */
$handler->display->display_options['relationships']['customer_id']['id'] = 'customer_id';
$handler->display->display_options['relationships']['customer_id']['table'] = 'shop';
$handler->display->display_options['relationships']['customer_id']['field'] = 'customer_id';
$handler->display->display_options['relationships']['customer_id']['relationship'] = 'shop_id';
$handler->display->display_options['relationships']['customer_id']['required'] = TRUE;
/* Relationship: Recce: User - Recce */
$handler->display->display_options['relationships']['created_by']['id'] = 'created_by';
$handler->display->display_options['relationships']['created_by']['table'] = 'recce';
$handler->display->display_options['relationships']['created_by']['field'] = 'created_by';
/* Field: Bulk operations: Recce */
$handler->display->display_options['fields']['views_bulk_operations']['id'] = 'views_bulk_operations';
$handler->display->display_options['fields']['views_bulk_operations']['table'] = 'views_entity_recce';
$handler->display->display_options['fields']['views_bulk_operations']['field'] = 'views_bulk_operations';
$handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['display_type'] = '0';
$handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['enable_select_all_pages'] = 1;
$handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['row_clickable'] = 1;
$handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['force_single'] = 0;
$handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['entity_load_capacity'] = '10';
$handler->display->display_options['fields']['views_bulk_operations']['vbo_operations'] = array(
  'action::vbo_recce_medium1_custom_action' => array(
    'selected' => 1,
    'postpone_processing' => 0,
    'skip_confirmation' => 1,
    'skip_permission_check' => 1,
    'override_label' => 0,
    'label' => '',
  ),
  'action::vbo_recce_medium2_custom_action' => array(
    'selected' => 1,
    'postpone_processing' => 0,
    'skip_confirmation' => 1,
    'skip_permission_check' => 1,
    'override_label' => 0,
    'label' => '',
  ),
);
/* Field: Shop: Shop ID */
$handler->display->display_options['fields']['shop_id']['id'] = 'shop_id';
$handler->display->display_options['fields']['shop_id']['table'] = 'shop';
$handler->display->display_options['fields']['shop_id']['field'] = 'shop_id';
$handler->display->display_options['fields']['shop_id']['relationship'] = 'shop_id';
$handler->display->display_options['fields']['shop_id']['exclude'] = TRUE;
$handler->display->display_options['fields']['shop_id']['separator'] = '';
/* Field: Shop: Outlet_name */
$handler->display->display_options['fields']['outlet_name']['id'] = 'outlet_name';
$handler->display->display_options['fields']['outlet_name']['table'] = 'shop';
$handler->display->display_options['fields']['outlet_name']['field'] = 'outlet_name';
$handler->display->display_options['fields']['outlet_name']['relationship'] = 'shop_id';
$handler->display->display_options['fields']['outlet_name']['label'] = 'Outlet Name';
/* Field: Village: District */
$handler->display->display_options['fields']['district']['id'] = 'district';
$handler->display->display_options['fields']['district']['table'] = 'village';
$handler->display->display_options['fields']['district']['field'] = 'district';
$handler->display->display_options['fields']['district']['relationship'] = 'village_id';
$handler->display->display_options['fields']['district']['label'] = 'Town';
/* Field: Recce: Created */
$handler->display->display_options['fields']['created']['id'] = 'created';
$handler->display->display_options['fields']['created']['table'] = 'recce';
$handler->display->display_options['fields']['created']['field'] = 'created';
$handler->display->display_options['fields']['created']['label'] = 'Recce Date';
$handler->display->display_options['fields']['created']['date_format'] = 'custom';
$handler->display->display_options['fields']['created']['custom_date_format'] = 'd-m-Y - H:i';
$handler->display->display_options['fields']['created']['second_date_format'] = 'long';
$handler->display->display_options['fields']['created']['format_date_sql'] = 0;
/* Field: Recce: Shop_image_id */
$handler->display->display_options['fields']['shop_image_id']['id'] = 'shop_image_id';
$handler->display->display_options['fields']['shop_image_id']['table'] = 'recce';
$handler->display->display_options['fields']['shop_image_id']['field'] = 'shop_image_id';
$handler->display->display_options['fields']['shop_image_id']['label'] = 'Recce Shop Image';
$handler->display->display_options['fields']['shop_image_id']['separator'] = '';
/* Field: Recce: Recce_image_id */
$handler->display->display_options['fields']['recce_image_id']['id'] = 'recce_image_id';
$handler->display->display_options['fields']['recce_image_id']['table'] = 'recce';
$handler->display->display_options['fields']['recce_image_id']['field'] = 'recce_image_id';
$handler->display->display_options['fields']['recce_image_id']['label'] = 'Recce Long Shot';
/* Field: Recce: Latitude */
$handler->display->display_options['fields']['latitude']['id'] = 'latitude';
$handler->display->display_options['fields']['latitude']['table'] = 'recce';
$handler->display->display_options['fields']['latitude']['field'] = 'latitude';
$handler->display->display_options['fields']['latitude']['exclude'] = TRUE;
/* Field: Recce: Longitude */
$handler->display->display_options['fields']['longitude']['id'] = 'longitude';
$handler->display->display_options['fields']['longitude']['table'] = 'recce';
$handler->display->display_options['fields']['longitude']['field'] = 'longitude';
$handler->display->display_options['fields']['longitude']['exclude'] = TRUE;
/* Field: Recce: Recce ID */
$handler->display->display_options['fields']['recce_id']['id'] = 'recce_id';
$handler->display->display_options['fields']['recce_id']['table'] = 'recce';
$handler->display->display_options['fields']['recce_id']['field'] = 'recce_id';
$handler->display->display_options['fields']['recce_id']['exclude'] = TRUE;
$handler->display->display_options['fields']['recce_id']['separator'] = '';
/* Field: Recce: Otp_sent */
$handler->display->display_options['fields']['otp_sent']['id'] = 'otp_sent';
$handler->display->display_options['fields']['otp_sent']['table'] = 'recce';
$handler->display->display_options['fields']['otp_sent']['field'] = 'otp_sent';
$handler->display->display_options['fields']['otp_sent']['exclude'] = TRUE;
/* Field: Global: PHP */
$handler->display->display_options['fields']['php']['id'] = 'php';
$handler->display->display_options['fields']['php']['table'] = 'views';
$handler->display->display_options['fields']['php']['field'] = 'php';
$handler->display->display_options['fields']['php']['label'] = 'Dealerboard';
$handler->display->display_options['fields']['php']['use_php_setup'] = 0;
$handler->display->display_options['fields']['php']['php_output'] = '<?php
echo ucfirst($row->recce_field6);
?>';
$handler->display->display_options['fields']['php']['use_php_click_sortable'] = '0';
$handler->display->display_options['fields']['php']['php_click_sortable'] = '';
/* Field: Recce: Recce_field11 */
$handler->display->display_options['fields']['recce_field11']['id'] = 'recce_field11';
$handler->display->display_options['fields']['recce_field11']['table'] = 'recce';
$handler->display->display_options['fields']['recce_field11']['field'] = 'recce_field11';
$handler->display->display_options['fields']['recce_field11']['label'] = 'Type';
/* Field: Recce: Recce_field12 */
$handler->display->display_options['fields']['recce_field12']['id'] = 'recce_field12';
$handler->display->display_options['fields']['recce_field12']['table'] = 'recce';
$handler->display->display_options['fields']['recce_field12']['field'] = 'recce_field12';
$handler->display->display_options['fields']['recce_field12']['label'] = 'Area in sq. ft.';
/* Field: Recce: Recce_field6 */
$handler->display->display_options['fields']['recce_field6']['id'] = 'recce_field6';
$handler->display->display_options['fields']['recce_field6']['table'] = 'recce';
$handler->display->display_options['fields']['recce_field6']['field'] = 'recce_field6';
$handler->display->display_options['fields']['recce_field6']['label'] = 'Action';
/* Field: Global: PHP */
$handler->display->display_options['fields']['php_1']['id'] = 'php_1';
$handler->display->display_options['fields']['php_1']['table'] = 'views';
$handler->display->display_options['fields']['php_1']['field'] = 'php';
$handler->display->display_options['fields']['php_1']['label'] = 'Flanges';
$handler->display->display_options['fields']['php_1']['use_php_setup'] = 0;
$handler->display->display_options['fields']['php_1']['php_output'] = '<?php
echo ucfirst($row->recce_field7);
?>';
$handler->display->display_options['fields']['php_1']['use_php_click_sortable'] = '0';
$handler->display->display_options['fields']['php_1']['php_click_sortable'] = '';
/* Field: Recce: Recce_field7 */
$handler->display->display_options['fields']['recce_field7']['id'] = 'recce_field7';
$handler->display->display_options['fields']['recce_field7']['table'] = 'recce';
$handler->display->display_options['fields']['recce_field7']['field'] = 'recce_field7';
$handler->display->display_options['fields']['recce_field7']['label'] = 'Action';
/* Field: Global: PHP */
$handler->display->display_options['fields']['php_2']['id'] = 'php_2';
$handler->display->display_options['fields']['php_2']['table'] = 'views';
$handler->display->display_options['fields']['php_2']['field'] = 'php';
$handler->display->display_options['fields']['php_2']['label'] = 'TBoard';
$handler->display->display_options['fields']['php_2']['use_php_setup'] = 0;
$handler->display->display_options['fields']['php_2']['php_output'] = '<?php
echo ucfirst($row->recce_field8);
?>';
$handler->display->display_options['fields']['php_2']['use_php_click_sortable'] = '0';
$handler->display->display_options['fields']['php_2']['php_click_sortable'] = '';
/* Field: Recce: Recce_field8 */
$handler->display->display_options['fields']['recce_field8']['id'] = 'recce_field8';
$handler->display->display_options['fields']['recce_field8']['table'] = 'recce';
$handler->display->display_options['fields']['recce_field8']['field'] = 'recce_field8';
$handler->display->display_options['fields']['recce_field8']['label'] = 'Action';
/* Field: Global: PHP */
$handler->display->display_options['fields']['php_3']['id'] = 'php_3';
$handler->display->display_options['fields']['php_3']['table'] = 'views';
$handler->display->display_options['fields']['php_3']['field'] = 'php';
$handler->display->display_options['fields']['php_3']['label'] = 'Khamba Pole';
$handler->display->display_options['fields']['php_3']['use_php_setup'] = 0;
$handler->display->display_options['fields']['php_3']['php_output'] = '<?php
echo ucfirst($row->recce_field9);
?>';
$handler->display->display_options['fields']['php_3']['use_php_click_sortable'] = '0';
$handler->display->display_options['fields']['php_3']['php_click_sortable'] = '';
/* Field: Recce: Recce_field9 */
$handler->display->display_options['fields']['recce_field9']['id'] = 'recce_field9';
$handler->display->display_options['fields']['recce_field9']['table'] = 'recce';
$handler->display->display_options['fields']['recce_field9']['field'] = 'recce_field9';
$handler->display->display_options['fields']['recce_field9']['label'] = 'Action';
/* Field: Global: PHP */
$handler->display->display_options['fields']['php_4']['id'] = 'php_4';
$handler->display->display_options['fields']['php_4']['table'] = 'views';
$handler->display->display_options['fields']['php_4']['field'] = 'php';
$handler->display->display_options['fields']['php_4']['label'] = 'Vinyl Sun Board';
$handler->display->display_options['fields']['php_4']['use_php_setup'] = 0;
$handler->display->display_options['fields']['php_4']['php_output'] = '<?php
echo ucfirst($row->recce_field10);
?>';
$handler->display->display_options['fields']['php_4']['use_php_click_sortable'] = '0';
$handler->display->display_options['fields']['php_4']['php_click_sortable'] = '';
/* Field: Recce: Recce_field13 */
$handler->display->display_options['fields']['recce_field13']['id'] = 'recce_field13';
$handler->display->display_options['fields']['recce_field13']['table'] = 'recce';
$handler->display->display_options['fields']['recce_field13']['field'] = 'recce_field13';
$handler->display->display_options['fields']['recce_field13']['label'] = 'Area in sq. ft.';
/* Field: Recce: Recce_field10 */
$handler->display->display_options['fields']['recce_field10']['id'] = 'recce_field10';
$handler->display->display_options['fields']['recce_field10']['table'] = 'recce';
$handler->display->display_options['fields']['recce_field10']['field'] = 'recce_field10';
$handler->display->display_options['fields']['recce_field10']['label'] = 'Action';
/* Field: Recce: Status */
$handler->display->display_options['fields']['status']['id'] = 'status';
$handler->display->display_options['fields']['status']['table'] = 'recce';
$handler->display->display_options['fields']['status']['field'] = 'status';
$handler->display->display_options['fields']['status']['exclude'] = TRUE;
/* Field: Recce: Status2 */
$handler->display->display_options['fields']['status2']['id'] = 'status2';
$handler->display->display_options['fields']['status2']['table'] = 'recce';
$handler->display->display_options['fields']['status2']['field'] = 'status2';
$handler->display->display_options['fields']['status2']['exclude'] = TRUE;
/* Field: Recce: Status3 */
$handler->display->display_options['fields']['status3']['id'] = 'status3';
$handler->display->display_options['fields']['status3']['table'] = 'recce';
$handler->display->display_options['fields']['status3']['field'] = 'status3';
$handler->display->display_options['fields']['status3']['exclude'] = TRUE;
/* Field: Recce: Status4 */
$handler->display->display_options['fields']['status4']['id'] = 'status4';
$handler->display->display_options['fields']['status4']['table'] = 'recce';
$handler->display->display_options['fields']['status4']['field'] = 'status4';
$handler->display->display_options['fields']['status4']['exclude'] = TRUE;
/* Field: Recce: Status5 */
$handler->display->display_options['fields']['status5']['id'] = 'status5';
$handler->display->display_options['fields']['status5']['table'] = 'recce';
$handler->display->display_options['fields']['status5']['field'] = 'status5';
$handler->display->display_options['fields']['status5']['exclude'] = TRUE;
/* Sort criterion: Recce: Created */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'recce';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';
$handler->display->display_options['filter_groups']['groups'] = array(
  1 => 'AND',
  2 => 'OR',
);
/* Filter criterion: State: State ID */
$handler->display->display_options['filters']['state_id']['id'] = 'state_id';
$handler->display->display_options['filters']['state_id']['table'] = 'state';
$handler->display->display_options['filters']['state_id']['field'] = 'state_id';
$handler->display->display_options['filters']['state_id']['relationship'] = 'state_id';
$handler->display->display_options['filters']['state_id']['group'] = 1;
$handler->display->display_options['filters']['state_id']['exposed'] = TRUE;
$handler->display->display_options['filters']['state_id']['expose']['operator_id'] = 'state_id_op';
$handler->display->display_options['filters']['state_id']['expose']['label'] = 'State';
$handler->display->display_options['filters']['state_id']['expose']['operator'] = 'state_id_op';
$handler->display->display_options['filters']['state_id']['expose']['identifier'] = 'state_id';
$handler->display->display_options['filters']['state_id']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
  4 => 0,
  5 => 0,
  6 => 0,
  7 => 0,
  8 => 0,
  9 => 0,
);
/* Filter criterion: Village: District */
$handler->display->display_options['filters']['district']['id'] = 'district';
$handler->display->display_options['filters']['district']['table'] = 'village';
$handler->display->display_options['filters']['district']['field'] = 'district';
$handler->display->display_options['filters']['district']['relationship'] = 'village_id';
$handler->display->display_options['filters']['district']['group'] = 1;
$handler->display->display_options['filters']['district']['exposed'] = TRUE;
$handler->display->display_options['filters']['district']['expose']['operator_id'] = 'district_op';
$handler->display->display_options['filters']['district']['expose']['label'] = 'Town';
$handler->display->display_options['filters']['district']['expose']['operator'] = 'district_op';
$handler->display->display_options['filters']['district']['expose']['identifier'] = 'district';
$handler->display->display_options['filters']['district']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
  4 => 0,
  5 => 0,
  6 => 0,
  7 => 0,
);
/* Filter criterion: Shop: Customer_id */
$handler->display->display_options['filters']['customer_id']['id'] = 'customer_id';
$handler->display->display_options['filters']['customer_id']['table'] = 'shop';
$handler->display->display_options['filters']['customer_id']['field'] = 'customer_id';
$handler->display->display_options['filters']['customer_id']['relationship'] = 'shop_id';
$handler->display->display_options['filters']['customer_id']['group'] = 1;
$handler->display->display_options['filters']['customer_id']['exposed'] = TRUE;
$handler->display->display_options['filters']['customer_id']['expose']['operator_id'] = 'customer_id_op';
$handler->display->display_options['filters']['customer_id']['expose']['label'] = 'Vendor';
$handler->display->display_options['filters']['customer_id']['expose']['operator'] = 'customer_id_op';
$handler->display->display_options['filters']['customer_id']['expose']['identifier'] = 'customer_id';
$handler->display->display_options['filters']['customer_id']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
  4 => 0,
  5 => 0,
  6 => 0,
  7 => 0,
);
/* Filter criterion: Recce: Recce_field1 */
$handler->display->display_options['filters']['recce_field1']['id'] = 'recce_field1';
$handler->display->display_options['filters']['recce_field1']['table'] = 'recce';
$handler->display->display_options['filters']['recce_field1']['field'] = 'recce_field1';
$handler->display->display_options['filters']['recce_field1']['group'] = 1;
$handler->display->display_options['filters']['recce_field1']['exposed'] = TRUE;
$handler->display->display_options['filters']['recce_field1']['expose']['operator_id'] = 'recce_field1_op';
$handler->display->display_options['filters']['recce_field1']['expose']['label'] = 'Element';
$handler->display->display_options['filters']['recce_field1']['expose']['operator'] = 'recce_field1_op';
$handler->display->display_options['filters']['recce_field1']['expose']['identifier'] = 'recce_field1';
$handler->display->display_options['filters']['recce_field1']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
  4 => 0,
  5 => 0,
  6 => 0,
  7 => 0,
  8 => 0,
  9 => 0,
);
/* Filter criterion: Shop: Town_src_flag */
$handler->display->display_options['filters']['town_src_flag']['id'] = 'town_src_flag';
$handler->display->display_options['filters']['town_src_flag']['table'] = 'shop';
$handler->display->display_options['filters']['town_src_flag']['field'] = 'town_src_flag';
$handler->display->display_options['filters']['town_src_flag']['relationship'] = 'shop_id';
$handler->display->display_options['filters']['town_src_flag']['value']['value'] = '3';
$handler->display->display_options['filters']['town_src_flag']['group'] = 1;
/* Filter criterion: Recce: Status */
$handler->display->display_options['filters']['status_1']['id'] = 'status_1';
$handler->display->display_options['filters']['status_1']['table'] = 'recce';
$handler->display->display_options['filters']['status_1']['field'] = 'status';
$handler->display->display_options['filters']['status_1']['value']['value'] = '75';
$handler->display->display_options['filters']['status_1']['group'] = 2;
$handler->display->display_options['filters']['status_1']['expose']['operator_id'] = 'status_1_op';
$handler->display->display_options['filters']['status_1']['expose']['label'] = 'recce Status';
$handler->display->display_options['filters']['status_1']['expose']['operator'] = 'status_1_op';
$handler->display->display_options['filters']['status_1']['expose']['identifier'] = 'status_1';
$handler->display->display_options['filters']['status_1']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
  4 => 0,
  5 => 0,
  6 => 0,
  7 => 0,
);
/* Filter criterion: Recce: Status2 */
$handler->display->display_options['filters']['status2']['id'] = 'status2';
$handler->display->display_options['filters']['status2']['table'] = 'recce';
$handler->display->display_options['filters']['status2']['field'] = 'status2';
$handler->display->display_options['filters']['status2']['value']['value'] = '75';
$handler->display->display_options['filters']['status2']['group'] = 2;
/* Filter criterion: Recce: Status3 */
$handler->display->display_options['filters']['status3']['id'] = 'status3';
$handler->display->display_options['filters']['status3']['table'] = 'recce';
$handler->display->display_options['filters']['status3']['field'] = 'status3';
$handler->display->display_options['filters']['status3']['value']['value'] = '75';
$handler->display->display_options['filters']['status3']['group'] = 2;
/* Filter criterion: Recce: Status4 */
$handler->display->display_options['filters']['status4']['id'] = 'status4';
$handler->display->display_options['filters']['status4']['table'] = 'recce';
$handler->display->display_options['filters']['status4']['field'] = 'status4';
$handler->display->display_options['filters']['status4']['value']['value'] = '75';
$handler->display->display_options['filters']['status4']['group'] = 2;
/* Filter criterion: Recce: Status5 */
$handler->display->display_options['filters']['status5']['id'] = 'status5';
$handler->display->display_options['filters']['status5']['table'] = 'recce';
$handler->display->display_options['filters']['status5']['field'] = 'status5';
$handler->display->display_options['filters']['status5']['value']['value'] = '75';
$handler->display->display_options['filters']['status5']['group'] = 2;

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'sit-and-eat-pending-recce-approval';
$handler->display->display_options['menu']['type'] = 'normal';
$handler->display->display_options['menu']['title'] = 'Recce Pending';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['name'] = 'main-menu';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;

/* Display: Data export */
$handler = $view->new_display('views_data_export', 'Data export', 'views_data_export_1');
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['style_plugin'] = 'views_data_export_csv';
$handler->display->display_options['defaults']['relationships'] = FALSE;
/* Relationship: Recce: Recce - Shop */
$handler->display->display_options['relationships']['shop_id']['id'] = 'shop_id';
$handler->display->display_options['relationships']['shop_id']['table'] = 'recce';
$handler->display->display_options['relationships']['shop_id']['field'] = 'shop_id';
$handler->display->display_options['relationships']['shop_id']['required'] = TRUE;
/* Relationship: Shop: Shop - Village */
$handler->display->display_options['relationships']['village_id']['id'] = 'village_id';
$handler->display->display_options['relationships']['village_id']['table'] = 'shop';
$handler->display->display_options['relationships']['village_id']['field'] = 'village_id';
$handler->display->display_options['relationships']['village_id']['relationship'] = 'shop_id';
$handler->display->display_options['relationships']['village_id']['required'] = TRUE;
/* Relationship: Village: Village - States */
$handler->display->display_options['relationships']['state_id']['id'] = 'state_id';
$handler->display->display_options['relationships']['state_id']['table'] = 'village';
$handler->display->display_options['relationships']['state_id']['field'] = 'state_id';
$handler->display->display_options['relationships']['state_id']['relationship'] = 'village_id';
$handler->display->display_options['relationships']['state_id']['required'] = TRUE;
/* Relationship: Shop: Shop - customer */
$handler->display->display_options['relationships']['customer_id']['id'] = 'customer_id';
$handler->display->display_options['relationships']['customer_id']['table'] = 'shop';
$handler->display->display_options['relationships']['customer_id']['field'] = 'customer_id';
$handler->display->display_options['relationships']['customer_id']['relationship'] = 'shop_id';
$handler->display->display_options['relationships']['customer_id']['required'] = TRUE;
/* Relationship: Recce: User - Recce */
$handler->display->display_options['relationships']['created_by']['id'] = 'created_by';
$handler->display->display_options['relationships']['created_by']['table'] = 'recce';
$handler->display->display_options['relationships']['created_by']['field'] = 'created_by';
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Shop: Shop ID */
$handler->display->display_options['fields']['shop_id']['id'] = 'shop_id';
$handler->display->display_options['fields']['shop_id']['table'] = 'shop';
$handler->display->display_options['fields']['shop_id']['field'] = 'shop_id';
$handler->display->display_options['fields']['shop_id']['relationship'] = 'shop_id';
$handler->display->display_options['fields']['shop_id']['exclude'] = TRUE;
$handler->display->display_options['fields']['shop_id']['separator'] = '';
/* Field: State: Name */
$handler->display->display_options['fields']['name_1']['id'] = 'name_1';
$handler->display->display_options['fields']['name_1']['table'] = 'state';
$handler->display->display_options['fields']['name_1']['field'] = 'name';
$handler->display->display_options['fields']['name_1']['relationship'] = 'state_id';
$handler->display->display_options['fields']['name_1']['label'] = 'State';
/* Field: Village: Village */
$handler->display->display_options['fields']['village']['id'] = 'village';
$handler->display->display_options['fields']['village']['table'] = 'village';
$handler->display->display_options['fields']['village']['field'] = 'village';
$handler->display->display_options['fields']['village']['relationship'] = 'village_id';
$handler->display->display_options['fields']['village']['label'] = 'Area';
/* Field: Village: District */
$handler->display->display_options['fields']['district']['id'] = 'district';
$handler->display->display_options['fields']['district']['table'] = 'village';
$handler->display->display_options['fields']['district']['field'] = 'district';
$handler->display->display_options['fields']['district']['relationship'] = 'village_id';
$handler->display->display_options['fields']['district']['label'] = 'Town';
/* Field: Shop: Asm */
$handler->display->display_options['fields']['asm']['id'] = 'asm';
$handler->display->display_options['fields']['asm']['table'] = 'shop';
$handler->display->display_options['fields']['asm']['field'] = 'asm';
$handler->display->display_options['fields']['asm']['relationship'] = 'shop_id';
/* Field: Customer: customer_name */
$handler->display->display_options['fields']['customer_name']['id'] = 'customer_name';
$handler->display->display_options['fields']['customer_name']['table'] = 'customer';
$handler->display->display_options['fields']['customer_name']['field'] = 'customer_name';
$handler->display->display_options['fields']['customer_name']['relationship'] = 'customer_id';
$handler->display->display_options['fields']['customer_name']['label'] = 'Vendor';
/* Field: Shop: Outlet_name */
$handler->display->display_options['fields']['outlet_name']['id'] = 'outlet_name';
$handler->display->display_options['fields']['outlet_name']['table'] = 'shop';
$handler->display->display_options['fields']['outlet_name']['field'] = 'outlet_name';
$handler->display->display_options['fields']['outlet_name']['relationship'] = 'shop_id';
$handler->display->display_options['fields']['outlet_name']['label'] = 'Outlet Name';
/* Field: Recce: Mobile_number */
$handler->display->display_options['fields']['mobile_number']['id'] = 'mobile_number';
$handler->display->display_options['fields']['mobile_number']['table'] = 'recce';
$handler->display->display_options['fields']['mobile_number']['field'] = 'mobile_number';
$handler->display->display_options['fields']['mobile_number']['label'] = 'Mobile';
/* Field: User: Name */
$handler->display->display_options['fields']['field_name']['id'] = 'field_name';
$handler->display->display_options['fields']['field_name']['table'] = 'field_data_field_name';
$handler->display->display_options['fields']['field_name']['field'] = 'field_name';
$handler->display->display_options['fields']['field_name']['relationship'] = 'created_by';
$handler->display->display_options['fields']['field_name']['label'] = 'Promoter';
$handler->display->display_options['fields']['field_name']['delta_offset'] = '0';
/* Field: Recce: Recce_field6 */
$handler->display->display_options['fields']['recce_field6']['id'] = 'recce_field6';
$handler->display->display_options['fields']['recce_field6']['table'] = 'recce';
$handler->display->display_options['fields']['recce_field6']['field'] = 'recce_field6';
$handler->display->display_options['fields']['recce_field6']['relationship'] = 'shop_id_1';
$handler->display->display_options['fields']['recce_field6']['label'] = 'Dealerboard';
$handler->display->display_options['fields']['recce_field6']['exclude'] = TRUE;
/* Field: Recce: Status */
$handler->display->display_options['fields']['status']['id'] = 'status';
$handler->display->display_options['fields']['status']['table'] = 'recce';
$handler->display->display_options['fields']['status']['field'] = 'status';
$handler->display->display_options['fields']['status']['relationship'] = 'shop_id_1';
$handler->display->display_options['fields']['status']['exclude'] = TRUE;
/* Field: Recce: Recce_field7 */
$handler->display->display_options['fields']['recce_field7']['id'] = 'recce_field7';
$handler->display->display_options['fields']['recce_field7']['table'] = 'recce';
$handler->display->display_options['fields']['recce_field7']['field'] = 'recce_field7';
$handler->display->display_options['fields']['recce_field7']['relationship'] = 'shop_id_1';
$handler->display->display_options['fields']['recce_field7']['label'] = 'Flanges';
$handler->display->display_options['fields']['recce_field7']['exclude'] = TRUE;
/* Field: Recce: Status2 */
$handler->display->display_options['fields']['status2']['id'] = 'status2';
$handler->display->display_options['fields']['status2']['table'] = 'recce';
$handler->display->display_options['fields']['status2']['field'] = 'status2';
$handler->display->display_options['fields']['status2']['relationship'] = 'shop_id_1';
$handler->display->display_options['fields']['status2']['exclude'] = TRUE;
/* Field: Recce: Recce_field8 */
$handler->display->display_options['fields']['recce_field8']['id'] = 'recce_field8';
$handler->display->display_options['fields']['recce_field8']['table'] = 'recce';
$handler->display->display_options['fields']['recce_field8']['field'] = 'recce_field8';
$handler->display->display_options['fields']['recce_field8']['relationship'] = 'shop_id_1';
$handler->display->display_options['fields']['recce_field8']['label'] = 'TBoard';
$handler->display->display_options['fields']['recce_field8']['exclude'] = TRUE;
/* Field: Recce: Status3 */
$handler->display->display_options['fields']['status3']['id'] = 'status3';
$handler->display->display_options['fields']['status3']['table'] = 'recce';
$handler->display->display_options['fields']['status3']['field'] = 'status3';
$handler->display->display_options['fields']['status3']['relationship'] = 'shop_id_1';
$handler->display->display_options['fields']['status3']['exclude'] = TRUE;
/* Field: Recce: Recce_field9 */
$handler->display->display_options['fields']['recce_field9']['id'] = 'recce_field9';
$handler->display->display_options['fields']['recce_field9']['table'] = 'recce';
$handler->display->display_options['fields']['recce_field9']['field'] = 'recce_field9';
$handler->display->display_options['fields']['recce_field9']['relationship'] = 'shop_id_1';
$handler->display->display_options['fields']['recce_field9']['label'] = 'Khamba Pole';
$handler->display->display_options['fields']['recce_field9']['exclude'] = TRUE;
/* Field: Recce: Status4 */
$handler->display->display_options['fields']['status4']['id'] = 'status4';
$handler->display->display_options['fields']['status4']['table'] = 'recce';
$handler->display->display_options['fields']['status4']['field'] = 'status4';
$handler->display->display_options['fields']['status4']['relationship'] = 'shop_id_1';
$handler->display->display_options['fields']['status4']['exclude'] = TRUE;
/* Field: Recce: Recce_field10 */
$handler->display->display_options['fields']['recce_field10']['id'] = 'recce_field10';
$handler->display->display_options['fields']['recce_field10']['table'] = 'recce';
$handler->display->display_options['fields']['recce_field10']['field'] = 'recce_field10';
$handler->display->display_options['fields']['recce_field10']['relationship'] = 'shop_id_1';
$handler->display->display_options['fields']['recce_field10']['label'] = 'Vinyl Sun Board';
$handler->display->display_options['fields']['recce_field10']['exclude'] = TRUE;
/* Field: Recce: Status5 */
$handler->display->display_options['fields']['status5']['id'] = 'status5';
$handler->display->display_options['fields']['status5']['table'] = 'recce';
$handler->display->display_options['fields']['status5']['field'] = 'status5';
$handler->display->display_options['fields']['status5']['relationship'] = 'shop_id_1';
$handler->display->display_options['fields']['status5']['exclude'] = TRUE;
/* Field: Global: PHP */
$handler->display->display_options['fields']['php_5']['id'] = 'php_5';
$handler->display->display_options['fields']['php_5']['table'] = 'views';
$handler->display->display_options['fields']['php_5']['field'] = 'php';
$handler->display->display_options['fields']['php_5']['label'] = 'Dealerboard';
$handler->display->display_options['fields']['php_5']['use_php_setup'] = 0;
$handler->display->display_options['fields']['php_5']['php_output'] = '<?php
echo ucFirst($row->recce_field6);
?>';
$handler->display->display_options['fields']['php_5']['use_php_click_sortable'] = '0';
$handler->display->display_options['fields']['php_5']['php_click_sortable'] = '';
/* Field: Recce: Recce_field11 */
$handler->display->display_options['fields']['recce_field11']['id'] = 'recce_field11';
$handler->display->display_options['fields']['recce_field11']['table'] = 'recce';
$handler->display->display_options['fields']['recce_field11']['field'] = 'recce_field11';
$handler->display->display_options['fields']['recce_field11']['label'] = 'Type';
/* Field: Recce: Recce_field12 */
$handler->display->display_options['fields']['recce_field12']['id'] = 'recce_field12';
$handler->display->display_options['fields']['recce_field12']['table'] = 'recce';
$handler->display->display_options['fields']['recce_field12']['field'] = 'recce_field12';
$handler->display->display_options['fields']['recce_field12']['label'] = 'Area in sq. ft.';
/* Field: Global: PHP */
$handler->display->display_options['fields']['php']['id'] = 'php';
$handler->display->display_options['fields']['php']['table'] = 'views';
$handler->display->display_options['fields']['php']['field'] = 'php';
$handler->display->display_options['fields']['php']['label'] = 'Dealerboard Status';
$handler->display->display_options['fields']['php']['use_php_setup'] = 0;
$handler->display->display_options['fields']['php']['php_output'] = '<?php

echo get_medium_status_name($row->status);

?>';
$handler->display->display_options['fields']['php']['use_php_click_sortable'] = '0';
$handler->display->display_options['fields']['php']['php_click_sortable'] = '';
/* Field: Global: PHP */
$handler->display->display_options['fields']['php_6']['id'] = 'php_6';
$handler->display->display_options['fields']['php_6']['table'] = 'views';
$handler->display->display_options['fields']['php_6']['field'] = 'php';
$handler->display->display_options['fields']['php_6']['label'] = 'Flanges';
$handler->display->display_options['fields']['php_6']['use_php_setup'] = 0;
$handler->display->display_options['fields']['php_6']['php_output'] = '<?php
echo ucfirst($row->recce_field7);
?>';
$handler->display->display_options['fields']['php_6']['use_php_click_sortable'] = '0';
$handler->display->display_options['fields']['php_6']['php_click_sortable'] = '';
/* Field: Global: PHP */
$handler->display->display_options['fields']['php_1']['id'] = 'php_1';
$handler->display->display_options['fields']['php_1']['table'] = 'views';
$handler->display->display_options['fields']['php_1']['field'] = 'php';
$handler->display->display_options['fields']['php_1']['label'] = 'Flanges Status';
$handler->display->display_options['fields']['php_1']['use_php_setup'] = 0;
$handler->display->display_options['fields']['php_1']['php_output'] = '<?php

echo get_medium_status_name($row->status2);


?>';
$handler->display->display_options['fields']['php_1']['use_php_click_sortable'] = '0';
$handler->display->display_options['fields']['php_1']['php_click_sortable'] = '';
/* Field: Global: PHP */
$handler->display->display_options['fields']['php_7']['id'] = 'php_7';
$handler->display->display_options['fields']['php_7']['table'] = 'views';
$handler->display->display_options['fields']['php_7']['field'] = 'php';
$handler->display->display_options['fields']['php_7']['label'] = 'TBoard';
$handler->display->display_options['fields']['php_7']['use_php_setup'] = 0;
$handler->display->display_options['fields']['php_7']['php_output'] = '<?php
echo ucfirst($row->recce_field8);
?>';
$handler->display->display_options['fields']['php_7']['use_php_click_sortable'] = '0';
$handler->display->display_options['fields']['php_7']['php_click_sortable'] = '';
/* Field: Global: PHP */
$handler->display->display_options['fields']['php_2']['id'] = 'php_2';
$handler->display->display_options['fields']['php_2']['table'] = 'views';
$handler->display->display_options['fields']['php_2']['field'] = 'php';
$handler->display->display_options['fields']['php_2']['label'] = 'TBoard Status';
$handler->display->display_options['fields']['php_2']['use_php_setup'] = 0;
$handler->display->display_options['fields']['php_2']['php_output'] = '<?php

echo get_medium_status_name($row->status3);


?>';
$handler->display->display_options['fields']['php_2']['use_php_click_sortable'] = '0';
$handler->display->display_options['fields']['php_2']['php_click_sortable'] = '';
/* Field: Global: PHP */
$handler->display->display_options['fields']['php_8']['id'] = 'php_8';
$handler->display->display_options['fields']['php_8']['table'] = 'views';
$handler->display->display_options['fields']['php_8']['field'] = 'php';
$handler->display->display_options['fields']['php_8']['label'] = 'Khamba Pole';
$handler->display->display_options['fields']['php_8']['use_php_setup'] = 0;
$handler->display->display_options['fields']['php_8']['php_output'] = '<?php
echo ucfirst($row->recce_field9);
?>';
$handler->display->display_options['fields']['php_8']['use_php_click_sortable'] = '0';
$handler->display->display_options['fields']['php_8']['php_click_sortable'] = '';
/* Field: Global: PHP */
$handler->display->display_options['fields']['php_3']['id'] = 'php_3';
$handler->display->display_options['fields']['php_3']['table'] = 'views';
$handler->display->display_options['fields']['php_3']['field'] = 'php';
$handler->display->display_options['fields']['php_3']['label'] = 'Khamba Pole Status';
$handler->display->display_options['fields']['php_3']['use_php_setup'] = 0;
$handler->display->display_options['fields']['php_3']['php_output'] = '<?php

echo get_medium_status_name($row->status4);


?>';
$handler->display->display_options['fields']['php_3']['use_php_click_sortable'] = '0';
$handler->display->display_options['fields']['php_3']['php_click_sortable'] = '';
/* Field: Global: PHP */
$handler->display->display_options['fields']['php_9']['id'] = 'php_9';
$handler->display->display_options['fields']['php_9']['table'] = 'views';
$handler->display->display_options['fields']['php_9']['field'] = 'php';
$handler->display->display_options['fields']['php_9']['label'] = 'Vinyl Sun Board';
$handler->display->display_options['fields']['php_9']['use_php_setup'] = 0;
$handler->display->display_options['fields']['php_9']['php_output'] = '<?php
echo ucfirst($row->recce_field10);
?>';
$handler->display->display_options['fields']['php_9']['use_php_click_sortable'] = '0';
$handler->display->display_options['fields']['php_9']['php_click_sortable'] = '';
/* Field: Recce: Recce_field13 */
$handler->display->display_options['fields']['recce_field13']['id'] = 'recce_field13';
$handler->display->display_options['fields']['recce_field13']['table'] = 'recce';
$handler->display->display_options['fields']['recce_field13']['field'] = 'recce_field13';
$handler->display->display_options['fields']['recce_field13']['label'] = 'Area in sq. ft.';
/* Field: Global: PHP */
$handler->display->display_options['fields']['php_4']['id'] = 'php_4';
$handler->display->display_options['fields']['php_4']['table'] = 'views';
$handler->display->display_options['fields']['php_4']['field'] = 'php';
$handler->display->display_options['fields']['php_4']['label'] = 'Vinyl Sun Board Status';
$handler->display->display_options['fields']['php_4']['use_php_setup'] = 0;
$handler->display->display_options['fields']['php_4']['php_output'] = '<?php

echo get_medium_status_name($row->status5);


?>';
$handler->display->display_options['fields']['php_4']['use_php_click_sortable'] = '0';
$handler->display->display_options['fields']['php_4']['php_click_sortable'] = '';
/* Field: Recce: Created */
$handler->display->display_options['fields']['created_1']['id'] = 'created_1';
$handler->display->display_options['fields']['created_1']['table'] = 'recce';
$handler->display->display_options['fields']['created_1']['field'] = 'created';
$handler->display->display_options['fields']['created_1']['relationship'] = 'shop_id_1';
$handler->display->display_options['fields']['created_1']['label'] = 'Recce Date';
$handler->display->display_options['fields']['created_1']['date_format'] = 'custom';
$handler->display->display_options['fields']['created_1']['custom_date_format'] = 'd/M/Y';
$handler->display->display_options['fields']['created_1']['second_date_format'] = 'long';
$handler->display->display_options['fields']['created_1']['format_date_sql'] = 0;
/* Field: Recce: Capture_time */
$handler->display->display_options['fields']['capture_time_1']['id'] = 'capture_time_1';
$handler->display->display_options['fields']['capture_time_1']['table'] = 'recce';
$handler->display->display_options['fields']['capture_time_1']['field'] = 'capture_time';
$handler->display->display_options['fields']['capture_time_1']['label'] = 'Recce Time';
/* Field: Recce: Latitude */
$handler->display->display_options['fields']['latitude']['id'] = 'latitude';
$handler->display->display_options['fields']['latitude']['table'] = 'recce';
$handler->display->display_options['fields']['latitude']['field'] = 'latitude';
$handler->display->display_options['fields']['latitude']['exclude'] = TRUE;
/* Field: Recce: Longitude */
$handler->display->display_options['fields']['longitude']['id'] = 'longitude';
$handler->display->display_options['fields']['longitude']['table'] = 'recce';
$handler->display->display_options['fields']['longitude']['field'] = 'longitude';
$handler->display->display_options['fields']['longitude']['exclude'] = TRUE;
/* Field: Global: PHP */
$handler->display->display_options['fields']['php_10']['id'] = 'php_10';
$handler->display->display_options['fields']['php_10']['table'] = 'views';
$handler->display->display_options['fields']['php_10']['field'] = 'php';
$handler->display->display_options['fields']['php_10']['label'] = 'Map Link';
$handler->display->display_options['fields']['php_10']['use_php_setup'] = 0;
$handler->display->display_options['fields']['php_10']['php_output'] = '<?php
echo "https://www.google.com/maps?q=".$row->latitude.",".$row->longitude;
?>';
$handler->display->display_options['fields']['php_10']['use_php_click_sortable'] = '0';
$handler->display->display_options['fields']['php_10']['php_click_sortable'] = '';
$handler->display->display_options['path'] = 'set-and-eat-all-recce.csv';
$handler->display->display_options['displays'] = array(
  'page' => 'page',
  'default' => 0,
);
$handler->display->display_options['use_batch'] = 'batch';
$handler->display->display_options['return_path'] = 'sit-and-eat-all-recce';
$handler->display->display_options['segment_size'] = '100';
