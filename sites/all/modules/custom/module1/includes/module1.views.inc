<?php

function module1_views_data() {
  // Basic table information.
  $data['userform_aa']['table']['group'] = t('User Form table');
  $data['userform_aa']['table']['base'] = array(
    // This is the identifier field for the view.
    'field' => 'id',
    'title' => t('User Form table'),
    'help' => t('User Form table contains example content and can be related to nodes.'),
    'weight' => -10,
  );
  $data['userform_aa']['id'] = array(
    'title' => t('User ID'),
    'help' => t('Just a numeric field.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  // Example plain text field.
  $data['userform_aa']['name'] = array(
    'title' => t('Name'),
    'help' => t('Just a plain text field.'),
    'field' => array(
      'handler' => 'views_handler_field',
      // This is use by the table display plugin.
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Example Address field.
  $data['userform_aa']['address'] = array(
    'title' => t('Address'),
    'help' => t('Just a Address field.'),
    'field' => array(
      'handler' => 'views_handler_field',
      // This is use by the table display plugin.
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Example Gender field.
    $data['userform_aa']['gender'] = array(
    'title' => t('Gender'),
    'help' => t('Just a Gender field.'),
    'field' => array(
      'handler' => 'views_handler_field',
      // This is use by the table display plugin.
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
    $data['userform_aa']['city'] = array(
    'title' => t('City'),
    'help' => t('Just a city field.'),
    'field' => array(
      'handler' => 'views_handler_field',
      // This is use by the table display plugin.
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
   
  // Example plain text field.
  $data['userform_aa']['created_at'] = array(
    'title' => t('Created At'),
    'help' => t('Just a Date field.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      // This is use by the table display plugin.
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_simple',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );
    // Example plain text field.
  $data['userform_aa']['updated_at'] = array(
    'title' => t('Updated At'),
    'help' => t('Just a Date field.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      // This is use by the table display plugin.
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_simple',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );
  
  return $data;

}


